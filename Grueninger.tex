\documentclass[ngerman]{dtk}%{article}
\ifluatex
\else
  \usepackage[utf8]{inputenc}
\fi

\begin{document}

%opening
\title{\LaTeX-Compiler in CMake-Projekten verwenden}
\Author{Christoph}{Grüninger}%
       {Sindelfingen\\
        \Email{foss@grueninger.de}}
\maketitle

\begin{abstract}
Bei größeren C++-Projekten setzt man gerne das Buildsystem CMake ein, um Compiler und Linker
flexibel und in unterschiedlichen Umgebungen einsetzen zu können. Wenn eine Dokumentation
mit \LaTeX{} besteht, soll diese meist ebenfalls mithilfe von CMake gebaut werden. Dafür bieten die
beiden Projekte UseLATEX.cmake und UseLatexMk alles notwendige. Der Artikel beschreibt wie
die Projekte zu verwenden sind.
\end{abstract}

% \begin{abstract}
% Larger C ++ projects often use the build system CMake to control compiler and linker
% in a flexible manner and in different environments. If a documentation based on
% \LaTeX{} is part of the project, it should be built by CMake, too. The two projects
% UseLATEX.cmake and UseLatexMk offer according functionality. The article describes
% how to use the projects.
% \end{abstract}

\LaTeX{} eignet sich hervorragend für begleitende Dokumente zu Software in Form von Handbüchern,
Dokumentationen oder Tutorials. Die oft genannten Stärken wie
Textsatz in hoher Qualität, leicht zu erstellende mathematische Formeln und unterschiedliche
Zielformate sprechen auch hier für dessen Einsatz.
Spezifische Vorteile in einem solchen Szenario sind der automatisierbare Bau der Dokumente, die
geringen Einstiegshürden in  \LaTeX{} für Programmierer im Vergleich zu anderen potentiellen Nutzern und dass
die \LaTeX-Quellen keine Binärdateien sind -- damit sind die Dateien kleiner und eignen sich wie der
Quelltext der Software selbst für den Einsatz einer Versionsverwaltung wie Subversion
oder Git.

Für im Quelltext verfügbare Software muss ein flexibler Build-Prozess bereitgestellt
werden, damit die Software auf den verschiedensten Installationen übersetzt werden kann.
Dies muss gelingen trotz optionaler Abhängigkeiten, unter Wahrung der Kompatibilität
zu mehreren Versionen von Werkzeugen und Bibliotheken, die wiederum unter nicht
einheitlichen Pfaden abgelegt sind. Da die im C++-Umfeld beliebten Make-Dateien dies schon
lange nicht mehr leisten können, gibt es Buildsysteme, die Make- oder vergleichbare
Dateien nach einem Konfigurationslauf erzeugen. In den letzten Jahren erfreut sich
das Buildsystem CMake\footnote{\url{www.cmake.org}} bei größeren C++-Projekten
wachsender Beliebtheit.

Wird damit die gesamte Konfiguration und Übersetzung der Software mit CMake gesteuert,
so kommt der Wunsch auf, den Bau der begleitenden \LaTeX-Dokumente aus CMake heraus zu
veranlassen. Übliche C++-Werkzeuge wie Compiler und Linker werden mit den passenden
Parametern aufgerufen und erzeugen im Idealfall keine Textausgabe. Nur Warnungen oder
Fehler werden ausgegeben, die dann vom Buildsystem gespeichert oder dem Nutzer angezeigt
werden. Im Gegensatz dazu muss man \LaTeX-Dokumente in mehreren Läufen übersetzen
und dazwischen andere Programme aufrufen, damit Referenzen, Inhaltsverzeichnisse,
Index, Nomenklatur etc. erstellt werden. Dabei ist nicht offensichtlich wie eine
Warnung zu beheben ist; abhängig von den im Dokument verwendeten Klassen müssen andere
Programme wie \BibTeX, \BibLaTeX{} oder Biber aufgerufen werden. Kleine Änderungen am
Dokument können zwischen einem und bis zu vier \LaTeX-Läufe nach sich ziehen.
Eine eigene Routine zu entwickeln, welche die Belange des eigenen Projekts abdecken,
tritt schnell eine Lawine von Korrekturen los, denn man schafft eine robuste und
portable Lösung kaum mit dem ersten Wurf. CMake selbst unterstützt nur
bei der Suche nach den Programmen \Program{latex}, \Program{pdflatex},
\Program{makeindex}, \Program{bibtex} und so weiter, nicht aber bei deren
Aufrufen, Parametern und der Reihenfolge.

Im Folgenden werden zwei Projekte vorgestellt, mit denen sich viele \LaTeX{}-Dokumente
komfortabel durch CMake gesteuert bauen lassen.

\section{UseLATEX.cmake}
Bei einer Suche zum Thema stößt man schnell auf UseLATEX.cmake\footnote{
\url{gitlab.kitware.com/kmorel/UseLATEX}}. 2005 im Wiki von CMake zum ersten Mal
veröffentlicht, hat es mit breiter Unterstützung einer großen Nutzerbasis
immer mehr Funktionen bekommen und es wurde um Fehler bereinigt. Es kann
Grafiken automatisch konvertieren, \LaTeX{} bei Bedarf mehrfach aufrufen und
unterstützt \LaTeX{} und \ BibTeX inklusive einer Reihe von Alternativen.
UseLATEX.cmake setzt die gesamte Logik zum Aufrufen der benötigten
Programme in CMake um.

Um es zu nutzen, muss nur die Datei UseLATEX.cmake innerhalb des CMake-Projekts abgelegt und
eingebunden werden. Dann kann es mit \lstinline{add_latex_document} und dem Dateinamen des
\LaTeX-Dokuments als Argument aufgerufen werden. Zusätzlich können optional nach
\lstinline{IMAGES} Bilder und nach \lstinline{BIBFILES} Bibliographie-Dateien angegeben werden. Es
können Angaben zum Name des Build-Target gemacht werden und ob es automatisch gebaut werden
soll oder explizit aufgerufen werden muss. So erzeugt der CMake-Code
\begin{lstlisting}
add_latex_document(
  dokument.tex
  BIBFILES referenzen.bib
  IMAGES bild0.png bild1.jpg
  USE_INDEX
  EXCLUDE_FROM_ALL)
\end{lstlisting}
ein Build-Target \lstinline{dokument}. Das Dokument wird nicht bei \lstinline{make} oder
\lstinline{make all} gebaut. Mit \lstinline{make dokument} wird aus \lstinline{dokument.tex} zusammen
mit den beiden Bildern und der Bibliographie-Datei ein PDF erzeugt.

Um den Umfang der Möglichkeiten zu umreißen ist die CMake-Funktion mit allen
möglichen Argumenten angegeben. Die Befehle sind weitgehend selbsterklärend.
Die Funktion ist in der CMake-üblichen Darstellung angegeben, wobei Worte in spitzen
Klammern für Namen, insbesondere Dateinamen, stehen und eckige Klammern
optionale Teile kennzeichnen:
\begin{lstlisting}
add_latex_document(<tex_file>
                   [BIBFILES <bib_files>]
                   [INPUTS <input_tex_files>]
                   [IMAGE_DIRS] <image_directories>
                   [IMAGES] <image_files>
                   [CONFIGURE] <tex_files>
                   [DEPENDS] <tex_files>
                   [MULTIBIB_NEWCITES] <suffix_list>
                   [USE_BIBLATEX]
                   [USE_INDEX]
                   [INDEX_NAMES <index_names>]
                   [USE_GLOSSARY] [USE_NOMENCL]
                   [FORCE_PDF] [FORCE_DVI] [FORCE_HTML]
                   [TARGET_NAME] <name>
                   [EXCLUDE_FROM_ALL]
                   [EXCLUDE_FROM_DEFAULTS])
\end{lstlisting}
Weitere Optionen und Funktionen, sowie weitere bereitgestellte
CMake"=Hilfsfunktionen sind in der umfangreichen Dokumentation
beschrieben.

\section{UseLatexMk}
Das Programm \Program{latexmk}\footnote{\url{personal.psu.edu/jcc8/latexmk/}}
ist ein Perl-Skript und automatisiert den Bau von \LaTeX-Dokumenten, indem
es die Ausgaben der beteiligten Programme parst und die passenden Aufrufe in der benötigten Anzahl
veranlasst. Entwickelt seit
1998 erfährt es noch immer regelmäßig Verbesserungen und Korrekturen.

Der Ansatz von UseLatexMk\footnote{\url{github.com/dokempf/UseLatexMk}} ist es, das
Rad nicht mit CMake neu zu erfinden, sondern \Program{latexmk} damit zu betrauen.
UseLatexMk ist noch jung und hat keine große Nutzerbasis, aber es greift auf
die Erfahrungen von \Program{latexmk} zurück und ist eine dünne CMake-Hülle
um den \Program{latexmk}-Aufruf. Es kommt dadurch mit deutlich weniger\footnote{
UseLatexMk kommt mit etwa 15\% der Quelltextzeilen aus, 252 statt 1777.}
eigenem Code aus, der Fehler enthalten kann oder Anpassungen nötig macht.

Ein \LaTeX{}-Dokument wird ebenfalls mit \lstinline{add_latex_document}
angelegt. Weitere \LaTeX{}-Dateien, Bilder, Bibliographie-Dateien oder
ähnliches müssen nicht angegeben werden, da \Program{latexmk} diese
selbst ermittelt. Obiges Beispiel würde mit UseLatexMk umgesetzt
werden als
\begin{lstlisting}
add_latex_document(
  SOURCE dokument.tex
  EXCLUDE_FROM_ALL)
\end{lstlisting}
und das Build-Target \lstinline{dokument_tex} erzeugen.

Die CMake-Funktion von UseLatexMk mit allen möglichen Argumenten
lautet:
\begin{lstlisting}
add_latex_document(SOURCE texsource
                   [TARGET target]
                   [EXCLUDE_FROM_ALL]
                   [REQUIRED]
                   [FATHER_TARGET father1 [father2 ...]]
                   [RCFILE rcfile1 [rcfile2 ...]]
                   [INSTALL destination]
                   [BUILD_ON_INSTALL])
\end{lstlisting}
Obwohl weniger Argumente aufgeführt werden, bietet UseLatexMk einen
vergleichbaren Funktionsumfang wie UseLATEX.cmake.

Mit RC-Dateien kann detailliert in den Bau der Dokumente eingegriffen werden;
es kann die Standard-RC-Datei von UseLatexMk verwendet werden. Zusätzlich
muss man mit \lstinline{FindLatexMk.cmake} eine Datei angeben, mit der \Program{latexmk}
gesucht und CMake bekannt gemacht wird. Dies ist bei CMake eine Routineaufgabe.

\section{Verwendung und Einordnung}
Für beide Ansätze muss nur eine bzw. drei CMake-Dateien ins
eigene Projekt kopiert und die entsprechende Datei eingebunden
werden. Beide Projekte stehen gleichermaßen unter einer BSD-Lizenz
(3-clause) und sollten in den meisten Fällen unproblematisch verwendet
werden können. Beide eigenen sich, um in Projekten, die CMake verwenden, \LaTeX-Quellen
zu übersetzen. Der konzeptionelle Unterschied ist lediglich, ob die Logik für
die richtigen Aufrufe der \LaTeX-Programme mit CMake oder außerhalb von CMake
umgesetzt wird. UseLatexMk erkennt Abhängigkeiten und ist deshalb für
Dokumente mit vielen Bildern und sonstigen zusätzlichen Dateien komfortabler
zu verwenden, da die Dateien nicht explizit angegeben werden müssen. Die Liste
von vorhandenen Bildern und anderen Dateien kann man sich aber auch mit wenigen
Zeilen CMake-Code erstellen lassen.

Trotz der guten Unterstützung durch UseLATEX.cmake und UseLatexMk verhält sich
\LaTeX{} in der CMake-Welt anders als die üblichen, im Idealfall schweigsamen Programmier-Werkzeuge: Es
erzeugt seitenweise Ausgaben aus mehrfachen Aufrufen, gespickt mit Warnungen bis
man das gewünschte Ergebnis erhält.

Generell scheint es einen Bedarf an einer \LaTeX-Build-Automatisierung
wie \Program{latexmk} zu geben. Wäre es nicht schön, wenn man
ohne weitere Hilfsmittel nur \Program{pdflatex} mit dem Pfad zum Master-Dokument
aufrufen müsste und der Rest könnte automatisch erledigt werden? Das
würde nicht nur das Leben von Entwicklern erleichtern, die \LaTeX-Dokumente
in CMake-Projekten erstellen wollen.
\end{document}
